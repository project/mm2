DESCRIPTION
--------------------------
Enables users to create Mailman lists from within Drupal.  The module is capable of creating and deleting lists, as well as allowing users to modify the description within Drupal.

INSTALLATION
--------------------------
- Patch Mailman with the Mailman XMLRPC patch included within this module directory
- Install this module as normal
- Configure Mailman2 settings in admin
- Create lists

TODO
--------------------------
- Allow users to edit description of list from within Drupal
- Add in functionality to allow (un)subscribing of lists
- Add in XMLRPC functions to replace the entire web cgi interface
- Create default views for listing mail lists
- Improve documentation
- Pass code-style.pl
